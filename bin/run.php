<?php
declare(strict_types=1);

require_once __DIR__.'/../vendor/autoload.php';

use Doctrine\Common\Annotations\AnnotationRegistry;
use Pfazzi\Timesheet\Infrastructure\Http\Server\Parameters;
use Pfazzi\Timesheet\Infrastructure\Http\Server\RequestHandler;
use Pfazzi\Timesheet\Infrastructure\Http\Server\Server;

AnnotationRegistry::registerLoader('class_exists');

$handler = new RequestHandler();
$handler->init();

$server= new Server(
    new Parameters(true, 8080),
    $handler
);

$server->run();
