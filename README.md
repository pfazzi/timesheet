### How to run
```bash
nodemon bin\run.php
```

### Description
Main patterns applied in the project:
* [X] [Command Query Responsibility Segregation](https://martinfowler.com/bliki/CQRS.html)
* [X] [Event Sourcing](https://martinfowler.com/eaaDev/EventSourcing.html)
* [X] [Repository](https://martinfowler.com/eaaCatalog/repository.html)
* [X] [Aggregate](https://martinfowler.com/bliki/DDD_Aggregate.html)
* [X] [Value Object](https://martinfowler.com/bliki/ValueObject.html)

