Feature: Update project
    As a user
    I want to update a project
    So that i can manage them

    Background:
        Given user "user@example.com" identified with password "aSecurePassword" exists
        And i am logged in as "user@example.com"
        And project "Shop" identified with id "dae63c6b-911a-415c-9001-3d9329a20260" exists and has been created "user@example.com"

    Scenario: Get Projects update via API
        When I add "Content-Type" header equal to "application/json"
        And I add the Authorization header
        And I add "Accept" header equal to "application/json"
        And I send a "GET" request to "/api/projects"
        Then the response status code should be 200
        And the response should be in JSON
        And the header "Content-Type" should be equal to "application/json"
        And print last JSON response
