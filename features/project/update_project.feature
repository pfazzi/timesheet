Feature: Update project
    As a user
    I want to update a project
    So that i can keep data up to date

    Background:
        Given user "user@example.com" identified with password "aSecurePassword" exists
        And i am logged in as "user@example.com"
        And project "Shop" identified with id "dae63c6b-911a-415c-9001-3d9329a20260" exists

    Scenario: Project update via API
        When I add "Content-Type" header equal to "application/json"
        And I add the Authorization header
        And I add "Accept" header equal to "application/json"
        And I send a "PUT" request to "/api/projects/dae63c6b-911a-415c-9001-3d9329a20260" with body:
        """
        {
          "name": "A wonderful Shop"
        }
        """
        Then the response status code should be 200
        And the response should be empty
