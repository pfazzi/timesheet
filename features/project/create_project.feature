Feature: Create project
    As a user
    I want to create a project
    So that i can record time i spend on it

    Background:
        Given user "user@example.com" identified with password "aSecurePassword" exists
        And i am logged in as "user@example.com"

    Scenario: Project creation via API
        When I add "Content-Type" header equal to "application/json"
        And I add the Authorization header
        And I add "Accept" header equal to "application/json"
        And I send a "POST" request to "/api/projects" with body:
        """
        {
          "id": "d12df307-056b-4b7d-8f0d-1d8123111c96",
          "name": "A new wonderful project"
        }
        """
        Then the response status code should be 201
        And the response should be empty
