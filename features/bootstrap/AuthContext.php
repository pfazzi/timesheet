<?php

use Behat\Behat\Context\Context;
use Behatch\HttpCall\Request;
use GuzzleHttp\Client;
use Pfazzi\Timesheet\Infrastructure\Behat\SharedStorage;
use Webmozart\Assert\Assert;

class AuthContext implements Context
{
    private Client $client;
    private Request $request;

    private array $passwords = [];

    private SharedStorage $storage;

    public function __construct(Request $request, SharedStorage $storage)
    {
        $this->client = new Client([
            'base_uri' => 'http://localhost:8080/', // TODO: muovere nella configurazione
        ]);

        $this->request = $request;
        $this->storage = $storage;
    }

    /**
     * @Given user :email identified with password :password exists
     */
    public function theUserIdentifiedWithPasswordExists(string $email, string $password)
    {
        $response = $this->client->post(
            '/api/user/register',
            [
                'json' => [
                    "email"    => $email,
                    "password" => $password,
                ]
            ]
        );

        Assert::eq(201, $response->getStatusCode(), 'Unable to register user');

        $this->passwords[$email] = $password;
    }

    /**
     * @Given i am logged in as :email
     */
    public function iAmLoggedInAs(string $email)
    {
        $response = $this->client->post(
            '/api/user/login',
            [
                'json' => [
                    "email"    => $email,
                    "password" => $this->passwords[$email],
                ]
            ]
        );

        $body = json_decode($response->getBody(), true);

        $this->storage->set('token', $body['token']);
    }


    /**
     * @When I add the Authorization header
     */
    public function iAddTheAuthenticationHeader()
    {
        $this->request->setHttpHeader('Authorization', 'Bearer ' . $this->storage->get('token'));
    }
}
