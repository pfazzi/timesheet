<?php
declare(strict_types=1);

use Behat\Behat\Context\Context;
use GuzzleHttp\Client;
use Pfazzi\Timesheet\Infrastructure\Behat\SharedStorage;
use Webmozart\Assert\Assert;

class ProjectContext implements Context
{

    private Client $client;
    private SharedStorage $storage;

    public function __construct(SharedStorage $storage)
    {
        $this->client = new Client([
            'base_uri' => 'http://localhost:8080/', // TODO: muovere nella configurazione
        ]);

        $this->storage = $storage;
    }

    /**
     * @Given project :name identified with id :id exists
     */
    public function projectIdentifiedWithIdExists(string $name, string $id)
    {
        $token = $this->storage->get('token');

        $response = $this->client->post(
            '/api/projects',
            [
                'headers' => [
                    'Authorization' => "Bearer $token"
                ],
                'json' => [
                    "id" => $id,
                    "name" => $name,
                ]
            ]
        );

        Assert::eq(201, $response->getStatusCode(), 'Unable to create project');
    }

}
