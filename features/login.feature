Feature: Authentication
    As an anonymous user
    I want to login
    So that i can use the app

    Background:
        Given user "user@example.com" identified with password "aSecurePassword" exists

    Scenario: Successful API login
        When I add "Content-Type" header equal to "application/json"
        And I add "Accept" header equal to "application/json"
        And I send a "POST" request to "/api/user/login" with body:
        """
        {
          "email": "user@example.com",
          "password": "aSecurePassword"
        }
        """
        Then the response status code should be 200
        And the response should be in JSON
        And the header "Content-Type" should be equal to "application/json"
        And the JSON node "token" should exist

    Scenario: API login with malformed request
        When I add "Content-Type" header equal to "application/json"
        And I add "Accept" header equal to "application/json"
        And I send a "POST" request to "/api/user/login" with body:
        """
        {
          "username": "user@test.com"
        }
        """
        Then the response status code should be 400
        And the response should be in JSON
        And the header "Content-Type" should be equal to "application/json"
        And the JSON node "errors.email" should exist
        And the JSON node "errors.password" should exist

    Scenario: API login with bad credentials
        When I add "Content-Type" header equal to "application/json"
        And I add "Accept" header equal to "application/json"
        And I send a "POST" request to "/api/user/login" with body:
        """
        {
          "email": "user@example.com",
          "password": "aWrongPassword"
        }
        """
        Then the response status code should be 401
        And the response should be empty
