<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Infrastructure\WriteModel;

use LogicException;
use Pfazzi\Timesheet\Domain\User\User;
use Pfazzi\Timesheet\Domain\User\UserRepository;
use Pfazzi\Timesheet\Infrastructure\Component\Clock\Clock;
use Pfazzi\Timesheet\Infrastructure\Component\EventSourcing\EventBus;
use Pfazzi\Timesheet\Infrastructure\Component\EventSourcing\EventStore;
use Pfazzi\Timesheet\Infrastructure\Component\EventSourcing\Repository;
use Ramsey\Uuid\UuidInterface;

class InMemoryUserRepository extends Repository implements UserRepository
{
    public function __construct(EventStore $store, EventBus $bus, Clock $clock)
    {
        parent::__construct(User::class, $store, $bus, $clock);
    }

    public function ofId(UuidInterface $id): User
    {
        $user = $this->load($id);

        if (!$user instanceof User) {
            throw new LogicException();
        }

        return $user;
    }

    public function add(User $user): void
    {
        $this->persist($user);
    }
}
