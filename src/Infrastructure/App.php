<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Infrastructure;

use DI\ContainerBuilder;
use Exception;
use Pfazzi\Timesheet\Infrastructure\Component\CommandBus\CommandBus;
use Psr\Container\ContainerInterface;

class App
{
    private ContainerInterface $container;

    /**
     * @throws Exception
     */
    private function buildContainer(): void
    {
        $definitions = require __DIR__ . '/Config/services.php';

        $builder = new ContainerBuilder();
        $builder->useAutowiring(true);
        $builder->addDefinitions($definitions);

        $this->container = $builder->build();
    }

    /**
     * @throws Exception
     */
    public function start(): void
    {
        $this->buildContainer();
    }

    public function dispatchCommand(object $command): void
    {
        $this->container
            ->get(CommandBus::class)
            ->dispatch($command);
    }
}
