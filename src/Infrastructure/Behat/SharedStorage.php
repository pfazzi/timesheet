<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Infrastructure\Behat;

use InvalidArgumentException;

/**
 * TODO: sintassi php 74
 */
class SharedStorage
{
    /** @var array */
    private $clipboard = [];

    /** @var string|null */
    private $latestKey;

    /**
     * {@inheritdoc}
     */
    public function get($key)
    {
        if (! isset($this->clipboard[$key])) {
            throw new InvalidArgumentException(sprintf('There is no current resource for "%s"!', $key));
        }

        return $this->clipboard[$key];
    }

    /**
     * {@inheritdoc}
     */
    public function has($key): bool
    {
        return isset($this->clipboard[$key]);
    }

    /**
     * {@inheritdoc}
     */
    public function set($key, $resource): void
    {
        $this->clipboard[$key] = $resource;
        $this->latestKey       = $key;
    }

    /**
     * {@inheritdoc}
     */
    public function getLatestResource()
    {
        if (! isset($this->clipboard[$this->latestKey])) {
            throw new InvalidArgumentException(sprintf('There is no "%s" latest resource!', $this->latestKey));
        }

        return $this->clipboard[$this->latestKey];
    }

    /**
     * {@inheritdoc}
     */
    public function setClipboard(array $clipboard): void
    {
        $this->clipboard = array_merge($this->clipboard, $clipboard);
    }
}
