<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Infrastructure\ReadModel\User;

use LogicException;
use Pfazzi\Timesheet\Domain\User\UserRegistered;
use Pfazzi\Timesheet\Infrastructure\Component\EventSourcing\Event;

class UserProjector
{
    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function __invoke(Event $event): void
    {
        $payload = $event->payload();

        switch (true) {
            case $payload instanceof UserRegistered:
                $newUser        = new User();
                $newUser->id    = $payload->id()->toString();
                $newUser->email = $payload->email()->toString();
                $this->userRepository->add($newUser);
                break;
            default:
                throw new LogicException(sprintf('Unable to project event "%s"', $event->type()));
        }
    }
}
