<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Infrastructure\ReadModel\User;

interface UserRepository
{
    public function getById(string $id): User;

    public function getByEmail(string $email): User;

    public function add(User $newUser);
}
