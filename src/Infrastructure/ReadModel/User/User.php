<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Infrastructure\ReadModel\User;

class User
{
    public string $id;
    public string $email;
}
