<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Infrastructure\ReadModel\User;

use Pfazzi\Timesheet\Domain\User\Email;
use Pfazzi\Timesheet\Domain\User\UserCollection;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use RuntimeException;

class InMemoryUserRepository implements UserRepository, UserCollection
{
    /**
     * @var User[]
     */
    private array $users = [];

    public function getById(string $id): User
    {
        foreach ($this->users as $user) {
            if ($user->id === $id) {
                return $user;
            }
        }

        // TODO: throw proper exception

        throw new RuntimeException(sprintf('User with id "%s" not found', $id));
    }

    public function getByEmail(string $email): User
    {
        foreach ($this->users as $user) {
            if ($user->email === $email) {
                return $user;
            }
        }

        // TODO: throw proper exception

        throw new RuntimeException(sprintf('User with email "%s" not found', $email));
    }

    public function add(User $newUser)
    {
        $this->users[] = $newUser;
    }

    public function getIdOfUserWithEmail(Email $email): UuidInterface
    {
        return Uuid::fromString($this->getByEmail($email->toString())->id);
    }
}
