<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Infrastructure\ReadModel\Project;

class Project
{
    public string $id;
    public string $name;
    public string $ownerId;

    public function __construct(string $ownerId, string $id, string $name)
    {
        $this->id   = $id;
        $this->name = $name;
        $this->ownerId = $ownerId;
    }
}
