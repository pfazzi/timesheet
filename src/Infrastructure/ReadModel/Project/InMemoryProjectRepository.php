<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Infrastructure\ReadModel\Project;

class InMemoryProjectRepository implements ProjectRepository
{
    /**
     * @var array<Project>
     */
    private array $projects;

    public function getAllOfUser(string $userId): array
    {
        return array_filter(
            $this->projects,
            fn (Project $project): bool => $project->ownerId === $userId
        );
    }
}
