<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Infrastructure\ReadModel\Project;

interface ProjectRepository
{
    /**
     * @return Project[]
     */
    public function getAllOfUser(string $userId): array;
}
