<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Infrastructure\ReadModel\Issue;

class Issue
{
    public string $id;
    public string $name;
    public array $issues;
}
