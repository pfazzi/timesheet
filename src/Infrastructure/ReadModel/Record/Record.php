<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Infrastructure\ReadModel\Record;

class Record
{
    public string $id;
    public string $userId;
    public string $issueId;
    public string $date;
    public int $time;
}
