<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Infrastructure\Http\Auth\Jwt;

use Lcobucci\JWT\Builder;
use Pfazzi\Timesheet\Infrastructure\ReadModel\User\UserRepository;

class TokenIssuer
{
    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function issueFor(string $userEmail): string
    {
        $user = $this->userRepository->getByEmail($userEmail);

        $time = time();

        $token = (new Builder())->issuedAt($time)
                                ->expiresAt($time + 3600)
                                ->withClaim('user_id', $user->id)
                                ->getToken();

        return (string)$token;
    }
}
