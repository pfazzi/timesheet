<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Infrastructure\Http\Auth\Jwt;

use Lcobucci\JWT\Parser;
use Lcobucci\JWT\ValidationData;
use Pfazzi\Timesheet\Infrastructure\ReadModel\User\UserRepository;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use React\Http\Response;

class AuthMiddleware implements MiddlewareInterface
{
    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $token = $this->extractTokenFrom($request);
        if (empty($token)) {
            return new Response(401);
        }

        $jwt = (new Parser())->parse($token);

        $tokenIsValid = $jwt->validate(new ValidationData());
        if (!$tokenIsValid) {
            return new Response(401);
        }

        $id = $jwt->getClaim('user_id');
        $user = $this->userRepository->getById($id);

        $request = $request->withAttribute('auth_user', $user);

        return $handler->handle($request);
    }

    private function extractTokenFrom(ServerRequestInterface $request): string
    {
        $header = $request->getHeader('Authorization');

        if (empty($header)) {
            return '';
        }

        if (! preg_match('/(Bearer\s)(.+)/', $header[0], $matches)) {
            return '';
        }

        return $matches[2] ?? '';
    }
}
