<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Infrastructure\Http\Factory;

use Psr\Http\Message\ResponseInterface;
use React\Http\Response;

class JsonResponseFactory
{
    /**
     * @param mixed $value
     */
    public function create(int $status = 200, $body = null, array $headers = []): ResponseInterface
    {
        $headers = array_merge($headers, ['Content-Type' => 'application/json']);

        if (null !== $body) {
            $body = json_encode($body, JSON_THROW_ON_ERROR);
        }

        return new Response(
            $status,
            $headers,
            $body
        );
    }
}
