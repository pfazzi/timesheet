<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Infrastructure\Http\Serializer;

use JMS\Serializer\SerializerBuilder;

class SymfonySerializerAdapter implements Deserializer
{
    public function deserialize(string $data, string $type): object
    {
        $serializer = SerializerBuilder::create()->build();

        return $serializer->deserialize($data, $type, 'json');
    }
}
