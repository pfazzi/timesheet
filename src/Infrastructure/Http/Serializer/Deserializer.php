<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Infrastructure\Http\Serializer;

interface Deserializer
{
    public function deserialize(string $str, string $type): object;
}
