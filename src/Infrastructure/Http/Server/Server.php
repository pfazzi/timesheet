<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Infrastructure\Http\Server;

use Pfazzi\Timesheet\Infrastructure\Component\Cli\Console;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use React\EventLoop\Factory;
use React\Http\Response;
use Symfony\Component\ErrorHandler\Debug;
use Throwable;

class Server
{
    private RequestHandlerInterface $requestHandler;
    private Parameters $parameters;

    public function __construct(Parameters $parameters, RequestHandlerInterface $requestHandler)
    {
        $this->requestHandler = $requestHandler;
        $this->parameters     = $parameters;
    }

    public function run()
    {
        if ($this->parameters->debug()) {
            Debug::enable();
        }

        $loop = Factory::create();

        $socket = new \React\Socket\Server($this->parameters->port(), $loop);

        $server = new \React\Http\Server(function (ServerRequestInterface $request): ResponseInterface {
            try {
                return $this->requestHandler->handle($request);
            } catch (Throwable $throwable) {
                return new Response(500, [
                    'Content-Type' => 'application/json'
                ], json_encode([
                    'message' => $throwable->getMessage(),
                    'trace' => explode('#', $throwable->getTraceAsString()),
                ]));
            }
        });

        $server->listen($socket);

        Console::log("Server running at http://127.0.0.1:{$this->parameters->port()}");

        $loop->run();
    }
}
