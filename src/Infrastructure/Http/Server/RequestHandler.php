<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Infrastructure\Http\Server;

use DI\ContainerBuilder;
use Laminas\Stratigility\MiddlewarePipe;
use League\Route\Strategy\ApplicationStrategy;
use Northwoods\Broker\Broker;
use Pfazzi\Timesheet\Infrastructure\Http\ExceptionHandler\RestExceptionHandlerDecorator;
use Pfazzi\Timesheet\Infrastructure\Http\Router\FixedLeagueRouter;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use function Laminas\Stratigility\middleware;

class RequestHandler implements RequestHandlerInterface
{
    private ContainerInterface $container;
    private FixedLeagueRouter $router;

    public function init(): void
    {
        $this->buildContainer();
        $this->buildRouter();
    }

    private function buildContainer(): void
    {
        $applicationServices = require __DIR__ . '/../../Config/services.php';
        $httpServices        = require __DIR__ . '/../Config/services.php';
        $definitions         = array_merge($applicationServices, $httpServices);

        $builder = new ContainerBuilder();
        $builder->useAutowiring(true);
        $builder->addDefinitions($definitions);

        $this->container = $builder->build();
    }

    private function buildRouter(): void
    {
        $strategy = new ApplicationStrategy();
        $strategy->setContainer($this->container);

        $this->router = (new FixedLeagueRouter())
            ->setStrategy($strategy)
            ->middleware($this->container->get(RestExceptionHandlerDecorator::class));

        $routesBuilder = require __DIR__ . '/../Config/routes.php';
        $routesBuilder($this->router);
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        return $this->router->dispatch($request);
    }
}
