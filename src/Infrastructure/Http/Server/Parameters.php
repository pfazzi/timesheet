<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Infrastructure\Http\Server;

class Parameters
{
    private bool $debug;
    private int $port;

    public function __construct(bool $debug, int $port)
    {
        $this->debug = $debug;
        $this->port  = $port;
    }

    public function debug(): bool
    {
        return $this->debug;
    }

    public function port(): int
    {
        return $this->port;
    }
}
