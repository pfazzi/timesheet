<?php
declare(strict_types=1);

use League\Route\RouteGroup;
use League\Route\Router;
use Pfazzi\Timesheet\Infrastructure\Http\Auth\Jwt\AuthMiddleware;
use Pfazzi\Timesheet\Infrastructure\Http\Controller\Rest\Project\GetProjectsController;
use Pfazzi\Timesheet\Infrastructure\Http\Controller\Rest\User\LoginController;
use Pfazzi\Timesheet\Infrastructure\Http\Controller\Web\IndexController as WebIndexController;

function invoke_service(string $className): array
{
    return [$className, '__invoke'];
}

return function (Router $router): void {
    $router->post('/api/user/login', LoginController::class);
    $router->post('/api/user/register', invoke_service('controller.user.register'));

    $router->group('/api', function (RouteGroup $group) {
        $group->get('/projects', GetProjectsController::class);
        $group->post('/projects', invoke_service('controller.project.create_project'));
        $group->put('/projects/{id:uuid}', invoke_service('controller.project.update_project'));
    })->lazyMiddleware(AuthMiddleware::class);

    $router->get('/', WebIndexController::class);
};
