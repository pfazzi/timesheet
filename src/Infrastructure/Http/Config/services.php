<?php
declare(strict_types=1);

use Pfazzi\Timesheet\Domain\User\InvalidCredentials;
use Pfazzi\Timesheet\Infrastructure\Http\Auth\Jwt\AuthMiddleware;
use Pfazzi\Timesheet\Infrastructure\Http\Controller\Rest\Common\ConfigurableController;
use Pfazzi\Timesheet\Infrastructure\Http\Controller\Rest\Common\Configuration;
use Pfazzi\Timesheet\Infrastructure\Http\Controller\Rest\IndexController as RestIndexController;
use Pfazzi\Timesheet\Infrastructure\Http\Controller\Rest\Project\CreateProjectCommandFactory;
use Pfazzi\Timesheet\Infrastructure\Http\Controller\Rest\Project\CreateProjectRequest;
use Pfazzi\Timesheet\Infrastructure\Http\Controller\Rest\Project\GetProjectsController;
use Pfazzi\Timesheet\Infrastructure\Http\Controller\Rest\Project\UpdateProjectCommandFactory;
use Pfazzi\Timesheet\Infrastructure\Http\Controller\Rest\Project\UpdateProjectRequest;
use Pfazzi\Timesheet\Infrastructure\Http\Controller\Rest\User\RegisterCommandFactory;
use Pfazzi\Timesheet\Infrastructure\Http\Controller\Rest\User\RegisterRequest;
use Pfazzi\Timesheet\Infrastructure\Http\Controller\Web\IndexController as WebIndexController;
use Pfazzi\Timesheet\Infrastructure\Http\ExceptionHandler\InvalidCredentialsHandler;
use Pfazzi\Timesheet\Infrastructure\Http\ExceptionHandler\RestExceptionHandlerDecorator;
use Pfazzi\Timesheet\Infrastructure\Http\Factory\JsonResponseFactory;
use Pfazzi\Timesheet\Infrastructure\Http\Serializer\Deserializer;
use Pfazzi\Timesheet\Infrastructure\Http\Serializer\SymfonySerializerAdapter;
use Pfazzi\Timesheet\Infrastructure\Http\Validator\SymfonyValidatorAdapter;
use Pfazzi\Timesheet\Infrastructure\Http\Validator\Validator;
use function DI\autowire;

$other_services = [
    AuthMiddleware::class => autowire(AuthMiddleware::class),

    RestExceptionHandlerDecorator::class => autowire(RestExceptionHandlerDecorator::class)
        ->method('register', InvalidCredentials::class, new InvalidCredentialsHandler()),

    JsonResponseFactory::class => autowire(JsonResponseFactory::class),
    Deserializer::class        => autowire(SymfonySerializerAdapter::class),
    Validator::class           => autowire(SymfonyValidatorAdapter::class),
];

$web_controllers = [
    WebIndexController::class => autowire(WebIndexController::class),
];

$rest_controllers = [
    RestIndexController::class => autowire(RestIndexController::class),

    GetProjectsController::class => autowire(GetProjectsController::class),

    'controller.user.register' =>
        autowire(ConfigurableController::class)
            ->constructorParameter(
                'configuration',
                Configuration::create(
                    RegisterRequest::class,
                    [RegisterCommandFactory::class, 'createFrom'],
                    201
                )
            ),

    'controller.project.create_project' => autowire(ConfigurableController::class)
        ->constructorParameter(
            'configuration',
            Configuration::create(
                CreateProjectRequest::class,
                [CreateProjectCommandFactory::class, 'createFrom'],
                201
            )
        ),

    'controller.project.update_project' => autowire(ConfigurableController::class)
        ->constructorParameter(
            'configuration',
            Configuration::create(
                UpdateProjectRequest::class,
                [UpdateProjectCommandFactory::class, 'createFrom'],
                200
            )
        ),
];

return array_merge($other_services, $rest_controllers, $web_controllers);
