<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Infrastructure\Http\Validator;

use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\Validation;

class SymfonyValidatorAdapter implements Validator
{
    public function validate(object $object): array
    {
        $violations = Validation::createValidatorBuilder()
                                ->enableAnnotationMapping()
                                ->getValidator()
                                ->validate($object);

        $errors = [];
        foreach ($violations as $violation) {
            /** @var ConstraintViolationInterface $violation */
            $errors[$violation->getPropertyPath()] = $violation->getMessage();
        }

        return $errors;
    }
}
