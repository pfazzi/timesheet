<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Infrastructure\Http\Validator;

interface Validator
{
    public function validate(object $object): array;
}
