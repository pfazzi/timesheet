<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Infrastructure\Http\Controller\Rest;

use Pfazzi\Timesheet\Infrastructure\Http\Factory\JsonResponseFactory;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class IndexController
{
    private JsonResponseFactory $factory;

    public function __construct(JsonResponseFactory $factory)
    {
        $this->factory = $factory;
    }

    public function __invoke(RequestInterface $request): ResponseInterface
    {
        return $this->factory->create(
            200,
            ['ciao' => 'mondo'],
        );
    }
}
