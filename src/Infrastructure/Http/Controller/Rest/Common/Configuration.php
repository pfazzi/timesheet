<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Infrastructure\Http\Controller\Rest\Common;

class Configuration
{
    private string $requestDtoType;
    /** @var callable */
    private $commandFactory;
    private int $statusCode;

    public function __construct(string $requestDtoType, callable $commandFactory, int $statusCode = 200)
    {
        $this->requestDtoType = $requestDtoType;
        $this->commandFactory = $commandFactory;
        $this->statusCode     = $statusCode;
    }

    public function requestDtoType(): string
    {
        return $this->requestDtoType;
    }

    public function commandFactory(): callable
    {
        return $this->commandFactory;
    }

    public function statusCode(): int
    {
        return $this->statusCode;
    }

    public static function create(string $requestDtoType, callable $commandFactory, int $statusCode = 200): self
    {
        return new self($requestDtoType, $commandFactory, $statusCode);
    }
}
