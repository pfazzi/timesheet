<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Infrastructure\Http\Controller\Rest\Common;

use Pfazzi\Timesheet\Infrastructure\Component\CommandBus\CommandBus;
use Pfazzi\Timesheet\Infrastructure\Http\Factory\JsonResponseFactory;
use Pfazzi\Timesheet\Infrastructure\Http\Serializer\Deserializer;
use Pfazzi\Timesheet\Infrastructure\Http\Validator\Validator;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class ConfigurableController
{
    private JsonResponseFactory $responseFactory;
    private CommandBus $commandBus;
    private Deserializer $deserializer;
    private Validator $validator;
    private Configuration $configuration;

    public function __construct(
        Deserializer $deserializer,
        Validator $validator,
        JsonResponseFactory $responseFactory,
        CommandBus $commandBus,
        Configuration $configuration
    ) {
        $this->responseFactory = $responseFactory;
        $this->commandBus      = $commandBus;
        $this->deserializer    = $deserializer;
        $this->validator       = $validator;
        $this->configuration   = $configuration;
    }

    public function __invoke(ServerRequestInterface $request, array $routeParams): ResponseInterface
    {
        $requestDto = $this->deserializer->deserialize(
            $request->getBody()->getContents(),
            $this->configuration->requestDtoType()
        );

        $violations = $this->validator->validate($requestDto);

        if (count($violations) > 0) {
            return $this->buildErrorResponse($violations);
        }

        $command = ($this->configuration->commandFactory())($requestDto, $routeParams);
        $this->commandBus->dispatch($command);

        return $this->responseFactory->create($this->configuration->statusCode());
    }

    private function buildErrorResponse(array $violations): ResponseInterface
    {
        return $this->responseFactory->create(
            400,
            [
                'errors' => $violations
            ]
        );
    }
}
