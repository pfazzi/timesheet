<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Infrastructure\Http\Controller\Rest\User;

use Pfazzi\Timesheet\Application\Command\User\Register;
use Ramsey\Uuid\Uuid;

class RegisterCommandFactory
{
    public static function createFrom(RegisterRequest $requestDto): Register
    {
        return new Register(
            Uuid::uuid4()->toString(),
            $requestDto->email,
            $requestDto->password
        );
    }
}
