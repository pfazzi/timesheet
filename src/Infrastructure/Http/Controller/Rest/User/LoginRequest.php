<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Infrastructure\Http\Controller\Rest\User;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class LoginRequest
{
    /**
     * @Assert\NotBlank()
     *
     * @Serializer\Type("string")
     */
    public ?string $email;

    /**
     * @Assert\NotBlank()
     *
     * @Serializer\Type("string")
     */
    public ?string $password;
}
