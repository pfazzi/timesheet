<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Infrastructure\Http\Controller\Rest\User;

use Pfazzi\Timesheet\Application\Command\User\Login;
use Pfazzi\Timesheet\Infrastructure\Component\CommandBus\CommandBus;
use Pfazzi\Timesheet\Infrastructure\Http\Auth\Jwt\TokenIssuer;
use Pfazzi\Timesheet\Infrastructure\Http\Factory\JsonResponseFactory;
use Pfazzi\Timesheet\Infrastructure\Http\Serializer\Deserializer;
use Pfazzi\Timesheet\Infrastructure\Http\Validator\Validator;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class LoginController
{
    private JsonResponseFactory $responseFactory;
    private CommandBus $commandBus;
    private Deserializer $deserializer;
    private Validator $validator;
    private TokenIssuer $tokenIssuer;

    public function __construct(
        Deserializer $deserializer,
        Validator $validator,
        JsonResponseFactory $responseFactory,
        CommandBus $commandBus,
        TokenIssuer $tokenIssuer
    ) {
        $this->responseFactory = $responseFactory;
        $this->commandBus      = $commandBus;
        $this->deserializer    = $deserializer;
        $this->validator       = $validator;
        $this->tokenIssuer     = $tokenIssuer;
    }

    public function __invoke(ServerRequestInterface $request): ResponseInterface
    {
        $requestDto = $this->deserializer->deserialize($request->getBody()->getContents(), LoginRequest::class);

        $violations = $this->validator->validate($requestDto);

        if (count($violations) > 0) {
            return $this->buildErrorResponse($violations);
        }

        $this->commandBus->dispatch(new Login(
            $requestDto->email,
            $requestDto->password
        ));

        $token = $this->tokenIssuer->issueFor($requestDto->email);

        return $this->responseFactory->create(200, [
            'token' => $token
        ]);
    }

    private function buildErrorResponse(array $violations): ResponseInterface
    {
        return $this->responseFactory->create(
            400,
            [
                'errors' => $violations
            ]
        );
    }
}
