<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Infrastructure\Http\Controller\Rest\Project;

use Pfazzi\Timesheet\Application\Command\Project\DeleteProject;
use Pfazzi\Timesheet\Infrastructure\Component\CommandBus\CommandBus;
use Pfazzi\Timesheet\Infrastructure\Http\Factory\JsonResponseFactory;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class DeleteProjectController
{
    private JsonResponseFactory $responseFactory;
    private CommandBus $commandBus;

    public function __construct(JsonResponseFactory $responseFactory, CommandBus $commandBus)
    {
        $this->responseFactory = $responseFactory;
        $this->commandBus      = $commandBus;
    }

    public function __invoke(ServerRequestInterface $request, array $routeParams): ResponseInterface
    {
        $this->commandBus->dispatch(new DeleteProject(
            $routeParams['id']
        ));

        return $this->responseFactory->create();
    }
}
