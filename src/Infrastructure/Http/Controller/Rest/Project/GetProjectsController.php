<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Infrastructure\Http\Controller\Rest\Project;

use Pfazzi\Timesheet\Infrastructure\Http\Auth\Jwt\AuthUser;
use Pfazzi\Timesheet\Infrastructure\Http\Factory\JsonResponseFactory;
use Pfazzi\Timesheet\Infrastructure\ReadModel\Project\ProjectRepository;
use Pfazzi\Timesheet\Infrastructure\ReadModel\User\User;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class GetProjectsController
{
    private JsonResponseFactory $responseFactory;
    private ProjectRepository $projectRepository;

    public function __construct(JsonResponseFactory $responseFactory, ProjectRepository $projectRepository)
    {
        $this->responseFactory   = $responseFactory;
        $this->projectRepository = $projectRepository;
    }

    public function __invoke(ServerRequestInterface $request): ResponseInterface
    {
        /** @var User $user */
        $user = $request->getAttribute('auth_user');

        $users = $this->projectRepository->getAllOfUser($user->id);

        return $this->responseFactory->create(200, $users);
    }
}
