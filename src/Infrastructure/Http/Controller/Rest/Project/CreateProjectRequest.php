<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Infrastructure\Http\Controller\Rest\Project;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class CreateProjectRequest
{
    /**
     * @Assert\NotBlank()
     * @Assert\Uuid()
     *
     * @Serializer\Type("string")
     */
    public ?string $id;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(max="100")
     *
     * @Serializer\Type("string")
     */
    public ?string $name;
}
