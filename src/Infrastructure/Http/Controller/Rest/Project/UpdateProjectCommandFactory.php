<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Infrastructure\Http\Controller\Rest\Project;

use Pfazzi\Timesheet\Application\Command\Project\UpdateProject;

class UpdateProjectCommandFactory
{
    public static function createFrom(UpdateProjectRequest $request, array $routeParams): UpdateProject
    {
        return new UpdateProject(
            $routeParams['id'],
            $request->name
        );
    }
}
