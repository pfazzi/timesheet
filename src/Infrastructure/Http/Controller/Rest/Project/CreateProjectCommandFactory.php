<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Infrastructure\Http\Controller\Rest\Project;

use Pfazzi\Timesheet\Application\Command\Project\CreateProject;

class CreateProjectCommandFactory
{
    public static function createFrom(CreateProjectRequest $request): CreateProject
    {
        return new CreateProject(
            $request->id,
            $request->name
        );
    }
}
