<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Infrastructure\Http\Controller\Web;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use React\Http\Response;

class IndexController
{
    public function __invoke(RequestInterface $request): ResponseInterface
    {
        return new Response(200, [], 'Index');
    }
}
