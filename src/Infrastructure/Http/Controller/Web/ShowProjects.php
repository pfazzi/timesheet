<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Infrastructure\Http\Controller\Web;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class ShowProjects
{
    public function __invoke(RequestInterface $request): ResponseInterface
    {
    }
}
