<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Infrastructure\Http\ExceptionHandler;

use Exception;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use React\Http\Response;
use Throwable;

class RestExceptionHandlerDecorator implements MiddlewareInterface
{
    private array $handlers = [];

    public function register(string $exceptionType, callable $handler): void
    {
        $this->handlers[$exceptionType] = $handler;
    }

    /**
     * @throws Exception
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        try {
            return $handler->handle($request);
        } catch (Throwable $exception) {
            if (!$this->hasHandlerFor($exception)) {
                return new Response(500, [
                    'Content-Type' => 'application/json'
                ], json_encode([
                    'message' => $exception->getMessage(),
                    'trace' => explode('#', $exception->getTraceAsString()),
                ]));
            }

            return $this->getHandlerFor($exception)($exception);
        }
    }

    private function hasHandlerFor(Throwable $exception): bool
    {
        return isset($this->handlers[get_class($exception)]);
    }

    private function getHandlerFor(Throwable $exception): callable
    {
        return $this->handlers[get_class($exception)];
    }
}
