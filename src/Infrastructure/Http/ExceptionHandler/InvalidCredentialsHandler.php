<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Infrastructure\Http\ExceptionHandler;

use Pfazzi\Timesheet\Domain\User\InvalidCredentials;
use Psr\Http\Message\ResponseInterface;
use React\Http\Response;

class InvalidCredentialsHandler
{
    public function __invoke(InvalidCredentials $exception): ResponseInterface
    {
        return new Response(401);
    }
}
