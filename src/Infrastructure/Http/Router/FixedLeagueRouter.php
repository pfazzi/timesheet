<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Infrastructure\Http\Router;

use League\Route\Router;
use Psr\Http\Message\ServerRequestInterface;

class FixedLeagueRouter extends Router
{
    private bool $routesPrepared = false;

    protected function prepRoutes(ServerRequestInterface $request): void
    {
        if ($this->routesPrepared) {
            return;
        }

        parent::prepRoutes($request);

        $this->routesPrepared = true;
    }
}
