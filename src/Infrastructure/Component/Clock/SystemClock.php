<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Infrastructure\Component\Clock;

use DateTimeImmutable;

class SystemClock implements Clock
{
    public function now(): DateTimeImmutable
    {
        return new DateTimeImmutable();
    }
}
