<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Infrastructure\Component\Clock;

use DateTimeImmutable;

interface Clock
{
    public function now(): DateTimeImmutable;
}
