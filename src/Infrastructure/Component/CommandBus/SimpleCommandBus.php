<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Infrastructure\Component\CommandBus;

use Webmozart\Assert\Assert;

class SimpleCommandBus implements CommandBus
{
    private array $handlers = [];

    public function register(string $command, callable $handler): void
    {
        $this->handlers[$command][] = $handler;
    }

    public function dispatch(object $command): void
    {
        $commandFqcn = get_class($command);

        Assert::keyExists(
            $this->handlers,
            $commandFqcn,
            sprintf('Handler for command "%s" not found', $commandFqcn)
        );

        foreach ($this->handlers[$commandFqcn] as $handler) {
            $handler($command);
        }
    }
}
