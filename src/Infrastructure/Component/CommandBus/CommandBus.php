<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Infrastructure\Component\CommandBus;

interface CommandBus
{
    public function dispatch(object $command): void;
}
