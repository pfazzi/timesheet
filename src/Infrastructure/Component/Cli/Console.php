<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Infrastructure\Component\Cli;

class Console
{
    private const COLORS = [
        'red' => '0;31',
        'white' => '1;37',
    ];

    private const BACKGROUND_COLOR = [
        'black' => '40',
    ];

    public static function log(string $message, string $color = 'white', $backgroundColor = 'black'): void
    {
        echo sprintf(
            "\e[%s;%sm%s\e[0m\n",
            self::COLORS[$color],
            self::BACKGROUND_COLOR[$backgroundColor],
            $message
        );
    }

    public static function error(string $message): void
    {
        self::log($message, 'red');
    }
}
