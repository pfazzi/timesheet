<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Infrastructure\Component\EventSourcing;

class EventBus
{
    private array $observers = [];

    public function register(string $event, callable $listener): void
    {
        $this->observers[$event][] = $listener;
    }

    public function publish(Event ...$events): void
    {
        foreach ($events as $event) {
            $eventType = $event->type();
            $this->observers[$eventType] ??= [];

            foreach ($this->observers[$eventType] as $observer) {
                $observer($event);
            }
        }
    }
}
