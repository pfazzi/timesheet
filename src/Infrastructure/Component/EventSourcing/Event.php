<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Infrastructure\Component\EventSourcing;

use DateTimeImmutable;
use JsonSerializable;
use Ramsey\Uuid\UuidInterface;

class Event
{
    private UuidInterface $entityId;
    private DateTimeImmutable $occurredAt;
    private JsonSerializable $payload;

    public function __construct(UuidInterface $entityId, DateTimeImmutable $occurredAt, JsonSerializable $payload)
    {
        $this->entityId   = $entityId;
        $this->occurredAt = $occurredAt;
        $this->payload    = $payload;
    }

    public function entityId(): UuidInterface
    {
        return $this->entityId;
    }

    public function occurredAt(): DateTimeImmutable
    {
        return $this->occurredAt;
    }

    public function payload(): JsonSerializable
    {
        return $this->payload;
    }

    public function type(): string
    {
        return get_class($this->payload);
    }
}
