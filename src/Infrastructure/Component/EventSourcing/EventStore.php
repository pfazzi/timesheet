<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Infrastructure\Component\EventSourcing;

use Ramsey\Uuid\UuidInterface;

interface EventStore
{
    public function append(Event ...$events): void;

    /**
     * @return Event[]
     */
    public function load(UuidInterface $id): array;
}
