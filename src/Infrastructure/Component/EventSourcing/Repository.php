<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Infrastructure\Component\EventSourcing;

use Pfazzi\Timesheet\Domain\AggregateRoot;
use Pfazzi\Timesheet\Infrastructure\Component\Clock\Clock;
use Ramsey\Uuid\UuidInterface;

abstract class Repository
{
    private EventBus $bus;
    private EventStore $store;
    private string $aggregateRootClass;
    private Clock $clock;

    public function __construct(string $aggregateRootClass, EventStore $store, EventBus $bus, Clock $clock)
    {
        $this->bus = $bus;
        $this->store = $store;
        $this->aggregateRootClass = $aggregateRootClass;
        $this->clock = $clock;
    }

    protected function persist(AggregateRoot $aggregateRoot): void
    {
        $events = array_map(
            fn (object $event): Event => new Event($aggregateRoot->id(), $this->clock->now(), $event),
            $aggregateRoot->uncommittedEvents()
        );

        $this->store->append(...$events);
        $this->bus->publish(...$events);
    }

    protected function load(UuidInterface $id): AggregateRoot
    {
        $events = array_map(
            fn (Event $event): object => $event->payload(),
            $this->store->load($id)
        );

        return call_user_func([$this->aggregateRootClass, 'fromEvents'], $events);
    }
}
