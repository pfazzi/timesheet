<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Infrastructure\Component\EventSourcing;

use Ramsey\Uuid\UuidInterface;

class InMemoryEventStore implements EventStore
{
    /**
     * @var Event[]
     */
    private array $events = [];

    public function append(Event ...$events): void
    {
        $this->events = [...$this->events, ...$events];
    }

    public function load(UuidInterface $id): array
    {
        return array_values(array_filter(
            $this->events,
            fn (Event $event): bool => $event->entityId()->equals($id)
        ));
    }
}
