<?php
declare(strict_types=1);

use Pfazzi\Timesheet\Application\Command\User\Login;
use Pfazzi\Timesheet\Application\Command\User\LoginHandler;
use Pfazzi\Timesheet\Application\Command\User\Register;
use Pfazzi\Timesheet\Application\Command\User\RegisterHandler;
use Pfazzi\Timesheet\Domain\User\UserCollection;
use Pfazzi\Timesheet\Domain\User\UserRegistered;
use Pfazzi\Timesheet\Domain\User\UserRepository as WriteUserRepository;
use Pfazzi\Timesheet\Infrastructure\Component\Clock\Clock;
use Pfazzi\Timesheet\Infrastructure\Component\Clock\SystemClock;
use Pfazzi\Timesheet\Infrastructure\Component\CommandBus\CommandBus;
use Pfazzi\Timesheet\Infrastructure\Component\CommandBus\SimpleCommandBus;
use Pfazzi\Timesheet\Infrastructure\Component\EventSourcing\EventBus;
use Pfazzi\Timesheet\Infrastructure\Component\EventSourcing\EventStore;
use Pfazzi\Timesheet\Infrastructure\Component\EventSourcing\InMemoryEventStore;
use Pfazzi\Timesheet\Infrastructure\ReadModel\Project\InMemoryProjectRepository;
use Pfazzi\Timesheet\Infrastructure\ReadModel\Project\ProjectRepository as ReadProjectRepository;
use Pfazzi\Timesheet\Infrastructure\ReadModel\User\InMemoryUserRepository;
use Pfazzi\Timesheet\Infrastructure\ReadModel\User\UserProjector;
use Pfazzi\Timesheet\Infrastructure\ReadModel\User\UserRepository as ReadUserRepository;
use function DI\autowire;
use function DI\get;

return [
    Clock::class => autowire(SystemClock::class),

    CommandBus::class => autowire(SimpleCommandBus::class)
        ->method('register', Login::class, get(LoginHandler::class))
        ->method('register', Register::class, get(RegisterHandler::class)),

    EventBus::class => autowire(EventBus::class)
        ->method('register', UserRegistered::class, get(UserProjector::class)),

    EventStore::class => autowire(InMemoryEventStore::class),

    WriteUserRepository::class => autowire(\Pfazzi\Timesheet\Infrastructure\WriteModel\InMemoryUserRepository::class),
    InMemoryUserRepository::class => autowire(InMemoryUserRepository::class),
    ReadUserRepository::class => get(InMemoryUserRepository::class),
    UserCollection::class => get(InMemoryUserRepository::class),


    ReadProjectRepository::class => autowire(InMemoryProjectRepository::class),
];
