<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Application\Command\User;

use Pfazzi\Timesheet\Domain\User\Email;

class Login
{
    private Email $email;
    private string $plainPassword;

    public function __construct(string $email, string $plainPassword)
    {
        $this->email = Email::fromString($email);
        $this->plainPassword = $plainPassword;
    }

    public function plainPassword(): string
    {
        return $this->plainPassword;
    }

    public function email(): Email
    {
        return $this->email;
    }
}
