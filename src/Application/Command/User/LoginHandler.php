<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Application\Command\User;

use Pfazzi\Timesheet\Domain\User\UserCollection;
use Pfazzi\Timesheet\Domain\User\UserRepository;

class LoginHandler
{
    private UserCollection $userCollection;
    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository, UserCollection $userCollection)
    {
        $this->userCollection = $userCollection;
        $this->userRepository = $userRepository;
    }

    public function __invoke(Login $command): void
    {
        $id = $this->userCollection->getIdOfUserWithEmail($command->email());

        $user = $this->userRepository->ofId($id);

        $user->login($command->plainPassword());
    }
}
