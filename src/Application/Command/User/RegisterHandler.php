<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Application\Command\User;

use Pfazzi\Timesheet\Domain\User\User;
use Pfazzi\Timesheet\Domain\User\UserRepository;

class RegisterHandler
{
    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function __invoke(Register $command): void
    {
        $user = User::register(
            $command->id(),
            $command->email(),
            $command->passwordHash()
        );

        $this->userRepository->add($user);
    }
}
