<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Application\Command\User;

use Pfazzi\Timesheet\Domain\User\Email;
use Pfazzi\Timesheet\Domain\User\PasswordHash;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class Register
{
    private UuidInterface $id;
    private PasswordHash $passwordHash;
    private Email $email;

    public function __construct(string $id, string $email, string $plainPassword)
    {
        $this->id           = Uuid::fromString($id);
        $this->passwordHash = PasswordHash::fromPlainPassword($plainPassword);
        $this->email        = Email::fromString($email);
    }

    public function id(): UuidInterface
    {
        return $this->id;
    }

    public function passwordHash(): PasswordHash
    {
        return $this->passwordHash;
    }

    public function email(): Email
    {
        return $this->email;
    }
}
