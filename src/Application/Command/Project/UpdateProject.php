<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Application\Command\Project;

use Pfazzi\Timesheet\Domain\Project\Name;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class UpdateProject
{
    private UuidInterface $id;
    private Name $name;

    public function __construct(string $id, string $name)
    {
        $this->id   = Uuid::fromString($id);
        $this->name = Name::fromString($name);
    }

    public function id(): UuidInterface
    {
        return $this->id;
    }

    public function name(): Name
    {
        return $this->name;
    }
}
