<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Application\Command\Project;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class DeleteProject
{
    private UuidInterface $id;

    public function __construct(string $id)
    {
        $this->id = Uuid::fromString($id);
    }

    public function id(): UuidInterface
    {
        return $this->id;
    }
}
