<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Domain;

use Ramsey\Uuid\UuidInterface;

interface AggregateRoot
{
    public function id(): UuidInterface;

    public function uncommittedEvents(): array;

    public static function fromEvents(array $events): self;
}
