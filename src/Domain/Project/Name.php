<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Domain\Project;

class Name
{
    private string $name;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public static function fromString(string $name): self
    {
        return new self($name);
    }
}
