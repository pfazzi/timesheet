<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Domain;

use Closure;
use Traversable;
use Webmozart\Assert\Assert;
use function get_class;

trait AggregateRootTrait
{
    private array $uncommittedEvents;

    public function uncommittedEvents(): array
    {
        $events = $this->uncommittedEvents;

        $this->uncommittedEvents = [];

        return $events;
    }

    /**
     * @return Traversable<string, callable>
     */
    abstract protected function eventAppliers(): Traversable;

    private function recordAndApplyThat(object $event): void
    {
        $this->record($event);
        $this->apply($event);
    }

    private function record(object $event): void
    {
        $this->uncommittedEvents[] = $event;
    }

    private function apply(object $event): void
    {
        static $appliers;

        if (empty($appliers)) {
            $appliers = iterator_to_array($this->eventAppliers());
        }

        $eventFqcn = get_class($event);

        Assert::keyExists(
            $appliers,
            $eventFqcn,
            sprintf(
                'Expected event applier for event "%s" in class "%s". Add it to "%s::eventAppliers" method.',
                $eventFqcn,
                get_class($this),
                get_class($this),
            )
        );

        $handler = Closure::fromCallable($appliers[$eventFqcn]);

        $handler->call($this, $event);
    }

    public static function fromEvents(array $events): self
    {
        $aggregateRoot = new self();

        foreach ($events as $event) {
            $aggregateRoot->apply($event);
        }

        return $aggregateRoot;
    }
}
