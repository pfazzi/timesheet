<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Domain\User;

use JsonSerializable;
use Ramsey\Uuid\UuidInterface;

class UserRegistered implements JsonSerializable
{
    private UuidInterface $id;
    private Email $email;
    private PasswordHash $passwordHash;

    public function __construct(
        UuidInterface $id,
        Email $email,
        PasswordHash $passwordHash
    ) {
        $this->id    = $id;
        $this->email = $email;
        $this->passwordHash = $passwordHash;
    }

    public function id(): UuidInterface
    {
        return $this->id;
    }

    public function email(): Email
    {
        return $this->email;
    }

    public function passwordHash(): PasswordHash
    {
        return $this->passwordHash;
    }

    public function jsonSerialize()
    {
        // TODO: Implement jsonSerialize() method.
    }
}
