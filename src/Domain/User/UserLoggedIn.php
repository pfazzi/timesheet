<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Domain\User;

use JsonSerializable;
use Ramsey\Uuid\UuidInterface;

class UserLoggedIn implements JsonSerializable
{
    private UuidInterface $id;

    public function __construct(UuidInterface $userId)
    {
        $this->id = $userId;
    }

    public function id(): UuidInterface
    {
        return $this->id;
    }

    public function jsonSerialize()
    {
        // TODO: Implement jsonSerialize() method.
    }
}
