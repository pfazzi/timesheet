<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Domain\User;

class Email
{
    private string $email;

    public function __construct(string $email)
    {
        $this->email = $email;
    }

    public static function fromString(string $email): self
    {
        return new self($email);
    }

    public function toString(): string
    {
        return $this->email;
    }
}
