<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Domain\User;

use Pfazzi\Timesheet\Domain\AggregateRoot;
use Pfazzi\Timesheet\Domain\AggregateRootTrait;
use Ramsey\Uuid\UuidInterface;
use Traversable;

class User implements AggregateRoot
{
    use AggregateRootTrait;

    private UuidInterface $id;
    private Email $email;
    private PasswordHash $passwordHash;

    private function __construct()
    {
    }

    public static function register(UuidInterface $id, Email $email, PasswordHash $passwordHash): self
    {
        $newUser = new self();

        $newUser->recordAndApplyThat(new UserRegistered($id, $email, $passwordHash));

        return $newUser;
    }

    public function login(string $plainPassword): void
    {
        if (!password_verify($plainPassword, $this->passwordHash->toString())) {
            throw new InvalidCredentials();
        }

        $this->recordAndApplyThat(new UserLoggedIn($this->id));
    }

    protected function eventAppliers(): Traversable
    {
        yield UserRegistered::class => function (UserRegistered $event): void {
            $this->id = $event->id();
            $this->email = $event->email();
            $this->passwordHash = $event->passwordHash();
        };

        yield UserLoggedIn::class => function () {
            // TODO: trova un modo migliore di ignorare gli eventi
        };
    }

    public function id(): UuidInterface
    {
        return $this->id;
    }
}
