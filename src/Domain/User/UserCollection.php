<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Domain\User;

use Ramsey\Uuid\UuidInterface;

interface UserCollection
{
    public function getIdOfUserWithEmail(Email $email): UuidInterface;
}
