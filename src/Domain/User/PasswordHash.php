<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Domain\User;

class PasswordHash
{
    private string $hash;

    public function __construct(string $hash)
    {
        $this->hash = $hash;
    }

    public static function fromPlainPassword(string $plainPassword): self
    {
        return new self(password_hash($plainPassword, PASSWORD_DEFAULT));
    }

    public function toString(): string
    {
        return $this->hash;
    }
}
