<?php
declare(strict_types=1);

namespace Pfazzi\Timesheet\Domain\User;

use RuntimeException;

class InvalidCredentials extends RuntimeException
{
}
